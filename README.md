# Power Sensor Holders


## One-Sentence-Pitch

Power sensor holders for more consistent laser power measurements.


## How does it work?

Here are three simple 3d printed holders designed for standard [Thorlabs power sensors](https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=3328), aimed at positioning the sensor with enhanced accuracy and reproducibility, thus eliminating the need for tape. These straightforward 3D-printed components will make your laser or light power measurements more consistent.

# Lightsheet 7 

<img src="./photos/lightsheet7_Z1render.jpg" width=50%/> 
<img src="./photos/lightsheet7Photo.jpg" width=50%/> 

Use this with standard Photodiode Power Sensors (such as the S121C), which easily slot into the left or right side of the sample chamber mock-up/sensor holder to measure the light sheet power. 

Part [lightsheet7.stl](./parts/lightsheet7.stl) is for the lightsheet 7 and [lightsheetZ1.stl](./parts/lightsheetZ1.stl) should work for the Z1 (untested). 

# 96 wellplate insert sensor holder 

<img src="./photos/96wellplateInverted.jpg" width=50%/> 
<img src="./photos/96wellplateUpright.jpg" width=50%/> 


Use this with Slim Photodiode Power Sensors (we use model S130C).
The sensor can be fastened to the [holder](./parts/96wellplate.stl) using a [knob](./parts/96well_knob.stl). To assemble the knob, use a nylon M3 screw, cut to the correct length, and secure it to the knob with an M3 nut and superglue.
Note that the sensor is positioned quite far from the imaging plane in the inverted configuration.

# angled holder for slide inserts

If the 96-well plate version does not work because the sensor collides with components such as the piezo insert, this version might be suitable for you.

<img src="./photos/sensorHolderYann.jpg" width=50%/> 
<img src="./photos/yannRender.jpg" width=50%/> 

The [holder](./parts/slideinsert_angled.stl) is positioned directly in the slots of the slide insert, and the sensor is secured in place with a [clamp](./parts/slideinsert_clampdown.stl). The clamp screws into an M3 insert in the holder. Use an elastic nylon screw to limit the force applied to the sensor.


## Status

works. Z1 holder is untested as we do not have this system. 

## Acknowledgment

Many thanks to Tereza Belinova, Mohammad Goudarzi, and Yann Cesbron for each contributing original ideas and conducting testing on the 96-well plate, lightsheet 7/Z1, and slide insert, respectively.

This project has been made possible in part by CZI grant DAF2020-225401 and grant DOI https://doi.org/10.37921/120055ratwvi from the Chan Zuckerberg Initiative DAF, an advised fund of Silicon Valley Community Foundation (funder DOI 10.13039/100014989).